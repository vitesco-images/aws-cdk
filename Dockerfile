FROM node:14-alpine

# needed to run docker in docker on Amazon Linux 2 in CodeBuild projects
# See AWS Case: 9042843731 in account 0016
VOLUME /var/lib/docker

RUN mkdir /etc/docker && echo '{"experimental": true}' > /etc/docker/daemon.json

RUN apk add --update --no-cache \
      bash \
      docker \
      python3 \
      py3-pip \
      && \
    rm -rf /var/cache/apk/*


# Install CDK
RUN npm install -g aws-cdk@~1
RUN python3 --version
RUN python3 -m pip install --no-cache-dir --upgrade pip && \
    python3 -m pip install --no-cache-dir \
      aws-cdk.core \
      aws-cdk.pipelines \
      aws-cdk.aws-dynamodb \
      aws-cdk.aws-lambda-python \
      aws-cdk.aws-codepipeline \
      aws-cdk.aws-codepipeline-actions \
      aws-cdk.aws-sns \
      aws-cdk.aws-kms \
      aws-cdk.aws-lambda \
      aws-cdk.aws-codecommit \
      aws-cdk.aws-codebuild \
      aws-cdk.aws-codepipeline-actions \
      aws-cdk.aws-codestarnotifications \
      aws-cdk.aws-cloudformation \
      aws-cdk.aws-stepfunctions \
      aws-cdk.aws-stepfunctions-tasks \
      aws-cdk.aws-events-targets \
      aws-cdk.aws-events \
      aws-cdk.aws-iam \
      aws-cdk.aws-s3 \
      aws-cdk.aws-ec2 \
      aws-cdk.aws-sns-subscriptions && \
    rm -rf /root/.cache/pip/*

